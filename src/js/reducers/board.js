import {
  MOVE_PIECE, RETURN_PIECE,
} from 'Constants/ActionTypes';

const rows = 8;
const columns = 8;

function configureBoard(x, y) {
  const board = [];
  for (let i = 0; i < x; i += 1) {
    board[i] = [];
    for (let k = 0; k < y; k += 1) {
      board[i][k] = { value: null, color: null };
    }
  }
  return board;
}

const initialState = {
  position: configureBoard(rows, columns),
};

const board = (state = initialState, action) => {
  switch (action.type) {
    case MOVE_PIECE: {
      const newPosition = state.position.slice(0, state.position.length);
      newPosition[action.newCoord.y][action.newCoord.x] = {
        value: action.piece.value,
        color: action.piece.color,
      };
      if (typeof action.oldCoord.y !== 'object' && typeof action.oldCoord.x !== 'object') {
        newPosition[action.oldCoord.y][action.oldCoord.x] = {
          value: null,
          color: null,
        };
      }
      return {
        ...state,
        position: newPosition,
      };
    }
    case RETURN_PIECE: {
      const newPosition = state.position.slice(0, state.position.length);
      if (typeof action.oldCoord.y === 'object' && typeof action.oldCoord.x === 'object') return state;
      newPosition[action.oldCoord.y][action.oldCoord.x] = {
        value: null,
        color: null,
      };
      return {
        ...state,
        position: newPosition,
      };
    }
    default:
      return state;
  }
};

export default board;
