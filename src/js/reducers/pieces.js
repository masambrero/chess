import {
  MOVE_PIECE, RETURN_PIECE,
} from 'Constants/ActionTypes';

const initialState = {
  black: {
    king: 1,
    queen: Infinity,
    rook: Infinity,
    bishop: Infinity,
    knight: Infinity,
    pawn: Infinity,
  },
  white: {
    king: 1,
    queen: Infinity,
    rook: Infinity,
    bishop: Infinity,
    knight: Infinity,
    pawn: Infinity,
  },
};

const pieces = (state = initialState, action) => {
  switch (action.type) {
    case MOVE_PIECE: {
      if (typeof action.oldCoord.y !== 'object' && typeof action.oldCoord.x !== 'object') return state;
      const piece = action.piece;
      const newPieceCount = state[piece.color][piece.value] - 1;
      const newSide = Object.assign(state[piece.color], { [piece.value]: newPieceCount });
      return {
        ...state,
        [piece.color]: newSide,
      };
    }
    case RETURN_PIECE: {
      const piece = action.piece;
      const newPieceCount = state[piece.color][piece.value] + 1;
      const newSide = Object.assign(state[piece.color], { [piece.value]: newPieceCount });
      return {
        ...state,
        [piece.color]: newSide,
      };
    }
    default:
      return state;
  }
};

export default pieces;
