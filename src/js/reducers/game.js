import {
  SELECT_PIECE,
  REQUEST_DECK_SAVE,
  RESPONSE_DECK_SAVE,
  CHANGE_INFO,
} from 'Constants/ActionTypes';

const initialState = {
  movablePiece: {
    piece: {},
    currentCoord: {},
  },
  firstTurn: 'Белые',
  deckName: 'Партия номер один',
  isFetching: false,
};

const game = (state = initialState, action) => {
  switch (action.type) {
    case SELECT_PIECE: {
      const newMovablePiece = Object.assign(state.movablePiece, {
        piece: action.piece,
        currentCoord: action.currentCoord,
      });
      return {
        ...state,
        movablePiece: newMovablePiece,
      };
    }
    case CHANGE_INFO: {
      const { deckName, firstTurn } = action;
      return {
        ...state,
        deckName,
        firstTurn,
      };
    }
    case REQUEST_DECK_SAVE: {
      return {
        ...state,
        isFetching: true,
      };
    }
    case RESPONSE_DECK_SAVE: {
      return {
        ...state,
        isFetching: false,
      };
    }
    default:
      return state;
  }
};

export default game;
