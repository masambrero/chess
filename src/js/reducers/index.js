import { combineReducers } from 'redux';
import pieces from './pieces';
import board from './board';
import game from './game';
import modal from './modal';

const rootReducer = combineReducers({
  pieces,
  board,
  game,
  modal,
});

export default rootReducer;
