import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import Modal from 'Containers/Modal/Modal';

const MainFrame = (props) => {
  const renderFirstChild = (component) => {
    const childrenArray = React.Children.toArray(component.children);
    return childrenArray[0] || null;
  };

  return (
    <div className="app">
      <CSSTransitionGroup
        component="main"
        className="content"
        transitionName="page-change"
        transitionEnterTimeout={500}
        transitionLeaveTimeout={500}
      >
        { React.cloneElement(props.children, { key: props.location.pathname }) }
      </CSSTransitionGroup>
      <CSSTransitionGroup
        component={renderFirstChild}
        transitionName="modal"
        transitionEnterTimeout={1000}
        transitionLeaveTimeout={1000}
      >
        {props.modalShown ? <Modal /> : null}
      </CSSTransitionGroup>
    </div>
  );
};

MainFrame.propTypes = {
  children: PropTypes.node.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  modalShown: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  modalShown: state.modal.modalShown,
});

export default connect(
  mapStateToProps,
)(MainFrame);
