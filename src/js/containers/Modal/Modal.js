import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { hideModal } from 'Actions';
import './Modal.css';

class Modal extends Component {
  componentDidMount() {
    this.toggleBodyOverflow();
  }

  componentWillUnmount() {
    this.toggleBodyOverflow();
  }

  toggleBodyOverflow = () => {
    document.body.classList.toggle('modal-open');
  }

  render() {
    return (
      <div className="modal" >
        <div className="modal__overlay" />
        <div className="modal__info">
          <div className="modal-content">
            <div className="modal-content__header modal-header">
              <h2 className="modal-header__header">ого!</h2>
              <button
                className="modal-header__btn" onClick={this.props.hideModal}
              >
                &#9932;
              </button>
            </div>
            <div className="modal-content__body">
              <div className="quote">
                <img src="/images/karjakin.png" className="quote__img" alt="karjakin" />
                <div className="quote__text">
                  {this.props.modalInfo}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Modal.defaultProps = {
  modalInfo: null,
};

Modal.propTypes = {
  hideModal: PropTypes.func.isRequired,
  modalInfo: PropTypes.string,
};

const mapStateToProps = state => ({
  modalInfo: state.modal.modalInfo,
});

const mapDispatchToProps = dispatch => ({
  hideModal: bindActionCreators(hideModal, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Modal);
