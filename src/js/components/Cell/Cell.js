import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Cell.css';

class Cell extends Component {
  shouldComponentUpdate(nextProps) {
    return this.props.disabled !== nextProps.disabled;
  }

  onDragStart = () => {
    if (this.props.disabled) return;
    const currentCoord = { x: this.props.xCoord, y: this.props.yCoord };
    const piece = { value: this.props.value, color: this.props.color };
    this.props.shouldSelectPiece(piece, currentCoord);
  }

  render() {
    const { color, value } = this.props;
    const isDraggable = !this.props.disabled;
    const piece = color && value ? `cell--${color}-${value}` : 'cell--empty';
    return (
      <td
        className={`${this.props.styleNames} cell ${piece} ${isDraggable ? 'cell--draggable' : 'cell--disabled'}`}
        data-x={this.props.xCoord}
        data-y={this.props.yCoord}
        onMouseDown={this.onDragStart}
        onTouchStart={this.onDragStart}
      />
    );
  }
}

Cell.defaultProps = {
  value: null,
  color: null,
  xCoord: null,
  yCoord: null,
};

Cell.propTypes = {
  disabled: PropTypes.bool.isRequired,
  styleNames: PropTypes.string.isRequired,
  value: PropTypes.string,
  color: PropTypes.string,
  xCoord: PropTypes.number,
  yCoord: PropTypes.number,
  shouldSelectPiece: PropTypes.func.isRequired,
};

export default Cell;
