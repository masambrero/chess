import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './InfoTab.css';

class InfoTab extends Component {
  constructor(props) {
    super(props);
    this.state = { deckName: this.props.deckName, firstTurn: 'white' };
  }

  changeDeskName = (e) => {
    this.setState({ deckName: e.target.value });
  }

  changeFirstSide = (e) => {
    this.setState({ firstTurn: e.target.value });
  }

  saveDeck = () => {
    this.props.toggleInfoTab();
    const { deckName, firstTurn } = this.state;
    this.props.shouldSaveDeck(deckName, firstTurn);
  }

  render() {
    return (
      <div className="info-tab">
        <div className="info-tab__wrapper">
          <header className="info-tab__header">
            <h2>{this.props.deckName}</h2>
          </header>
          <div className="info-tab__desk-edit">
            <div className="desk-editor">
              <div className="desk-editor__input-group">
                <label
                  htmlFor="desk-editor__input--desk-name"
                  className="desk-editor__label"
                >
                  Название партии:
                </label>
                <input
                  id="desk-editor__input--desk-name"
                  className="desk-editor__input desk-editor__input--desk-name"
                  type="text"
                  placeholder={this.props.deckName}
                  name="deckName"
                  onChange={this.changeDeskName}
                />
              </div>
              <div className="desk-editor__input-group">
                <label
                  htmlFor="desk-editor__input--first-side"
                  className="desk-editor__label"
                >
                  Первый ход:
                </label>
                <select
                  id="desk-editor__input--first-side"
                  className="desk-editor__input desk-editor__input--first-side"
                  name="firstTurn"
                  onChange={this.changeFirstSide}
                >
                  <option name="white">Белые</option>
                  <option name="black">Черные</option>
                </select>
              </div>
              <button
                className="btn desk-editor__btn"
                onClick={this.saveDeck}
              >
                Сохранить
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

InfoTab.propTypes = {
  toggleInfoTab: PropTypes.func.isRequired,
  shouldSaveDeck: PropTypes.func.isRequired,
  deckName: PropTypes.string.isRequired,
};

export default InfoTab;
