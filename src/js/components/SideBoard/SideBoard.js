import React from 'react';
import PropTypes from 'prop-types';
import Cell from 'Components/Cell/Cell';
import './SideBoard.css';

const SideBoard = props => (
  (
    <div className={`chess__side-board chess__side-board--${props.color}`}>
      <table className="side-board">
        <tbody>
          <tr>
            {Object.keys(props.pieces).map(i => (
              <Cell
                key={i}
                color={props.color}
                value={i}
                disabled={!props.pieces[i]}
                styleNames="side-board__cell"
                shouldSelectPiece={props.shouldSelectPiece}
              />
            ))}
          </tr>
        </tbody>
      </table>
    </div>
  )
);

SideBoard.propTypes = {
  pieces: PropTypes.shape({
    king: PropTypes.number.isRequired,
    queen: PropTypes.number.isRequired,
    rook: PropTypes.number.isRequired,
    bishop: PropTypes.number.isRequired,
    knight: PropTypes.number.isRequired,
    pawn: PropTypes.number.isRequired,
  }).isRequired,
  color: PropTypes.string.isRequired,
  shouldSelectPiece: PropTypes.func.isRequired,
};

export default SideBoard;
