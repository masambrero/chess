import React from 'react';
import PropTypes from 'prop-types';
import Cell from 'Components/Cell/Cell';
import './Board.css';

const Board = (props) => {
  const { position } = props;
  return (
    <table className="board">
      <tbody>
        {position.map((y, yIndex) => (
          <tr key={yIndex}>
            {y.map((x, xIndex) => (
              <Cell
                key={xIndex}
                xCoord={xIndex}
                yCoord={yIndex}
                disabled={!x.value}
                value={x.value}
                color={x.color}
                styleNames="board__cell"
                shouldSelectPiece={props.shouldSelectPiece}
              />
            ))}
          </tr>
        )).reverse()}
      </tbody>
    </table>
  );
};

Board.propTypes = {
  position: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.string,
        color: PropTypes.string,
      }).isRequired,
    ).isRequired,
  ).isRequired,
};

export default Board;
