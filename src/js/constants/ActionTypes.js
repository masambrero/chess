export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const MOVE_PIECE = 'MOVE_PIECE';
export const RETURN_PIECE = 'RETURN_PIECE';
export const SELECT_PIECE = 'SELECT_PIECE';
export const CHANGE_INFO = 'CHANGE_INFO';
export const REQUEST_DECK_SAVE = 'REQUEST_DECK_SAVE';
export const RESPONSE_DECK_SAVE = 'RESPONSE_DECK_SAVE';
