import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import { shouldMovePiece, returnPiece, shouldSelectPiece, shouldSaveDeck } from 'Actions';
import SideBoard from 'Components/SideBoard/SideBoard';
import Board from 'Components/Board/Board';
import InfoTab from 'Components/InfoTab/InfoTab';
import './Chess.css';

class Chess extends Component {
  static renderFirstChild(component) {
    const childrenArray = React.Children.toArray(component.children);
    return childrenArray[0] || null;
  }

  static getEventCoord(e) {
    let clientX;
    let clientY;
    if (e.type.match(/mouse/)) {
      clientX = e.clientX;
      clientY = e.clientY;
    } else if (e.type.match(/touch/)) {
      clientX = e.changedTouches[0].clientX;
      clientY = e.changedTouches[0].clientY;
    } else {
      return false;
    }
    return { x: clientX, y: clientY };
  }

  constructor(props) {
    super(props);
    this.state = { infoTabShown: false };
    this.draggedPiece = {};
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.onDragStart);
    document.addEventListener('mousemove', this.onDragging);
    document.addEventListener('mouseup', this.onDragEnd);
    document.addEventListener('touchstart', this.onDragStart);
    document.addEventListener('touchmove', this.onDragging);
    document.addEventListener('touchend', this.onDragEnd);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.onDragStart);
    document.removeEventListener('mousemove', this.onDragging);
    document.removeEventListener('mouseup', this.onDragEnd);
    document.removeEventListener('touchstart', this.onDragStart);
    document.removeEventListener('touchmove', this.onDragging);
    document.removeEventListener('touchend', this.onDragEnd);
  }

  onDragStart = (e) => {
    if (!e.target.closest('.cell--draggable')) return false;
    const startCoord = Chess.getEventCoord(e);
    if (!startCoord) return false;
    const piece = {
      value: this.props.movablePiece.piece.value,
      color: this.props.movablePiece.piece.color,
    };
    const currentCoord = this.props.movablePiece.currentCoord;
    if (!piece) return false;
    this.draggedPiece.piece = piece;
    this.draggedPiece.currentCoord = currentCoord;
    this.draggedPiece.startCoords = { x: startCoord.x, y: startCoord.y };
    return false;
  };

  onDragging = (e) => {
    if (!this.draggedPiece.piece) return false;
    if (this.draggedPiece.img) window.getSelection().removeAllRanges();
    const currentCoord = Chess.getEventCoord(e);
    if (!currentCoord) return false;
    if (!this.draggedPiece.img) {
      const moveX = currentCoord.x - this.draggedPiece.startCoords.x;
      const moveY = currentCoord.y - this.draggedPiece.startCoords.y;
      if (Math.abs(moveX) < 2 && Math.abs(moveY) < 2) return false;
      this.draggedPiece.img = this.createDraggedImg(e);
      if (!this.draggedPiece.img) {
        this.draggedPiece = {};
        return false;
      }
    }
    this.draggedPiece.img.style.left = `${currentCoord.x - 20}px`;
    this.draggedPiece.img.style.top = `${currentCoord.y - 20}px`;
    return false;
  };

  onDragEnd = (e) => {
    if (this.draggedPiece.img) {
      const endCoord = Chess.getEventCoord(e);
      if (!endCoord) return false;
      const { piece, currentCoord } = this.draggedPiece;
      document.body.removeChild(this.draggedPiece.img);
      this.draggedPiece = {};
      const cell = document.elementFromPoint(endCoord.x, endCoord.y);
      if (!(cell.dataset.x && cell.dataset.y) && currentCoord) {
        return this.props.returnPiece(piece, currentCoord);
      }
      const newCoord = { x: cell.dataset.x, y: cell.dataset.y };
      this.props.shouldMovePiece(piece, newCoord, currentCoord);
    }
    return false;
  };

  createDraggedImg = () => {
    const { value, color } = this.draggedPiece.piece;
    const draggedImg = document.createElement('div');
    draggedImg.className = `draggedPiece draggedPiece--${color}-${value}`;
    this.draggedPiece.img = draggedImg;
    document.body.appendChild(draggedImg);
    return draggedImg;
  };

  toggleInfoTab = () => {
    this.setState({ infoTabShown: !this.state.infoTabShown });
  };

  render() {
    const infoTabShown = this.state.infoTabShown;
    return (
      <div className="chess">
        <div className="chess__main-tab">
          {Object.keys(this.props.pieces).map(i => (
            <SideBoard
              key={i}
              color={i}
              pieces={this.props.pieces[i]}
              shouldSelectPiece={this.props.shouldSelectPiece}
              movablePiece={this.props.movablePiece}
            />
          ))}
          <div className="chess__board">
            <Board
              position={this.props.position}
              shouldSelectPiece={this.props.shouldSelectPiece}
            />
          </div>
        </div>
        <div className="chess__info-button">
          <button className="btn" onClick={this.toggleInfoTab}>
            сохранить
          </button>
        </div>
        <CSSTransitionGroup
          component={this.renderFirstChild}
          transitionName="tab-show"
          transitionEnterTimeout={700}
          transitionLeaveTimeout={700}
        >
          {infoTabShown && (
            <InfoTab
              deckName={this.props.deckName}
              shouldSaveDeck={this.props.shouldSaveDeck}
              toggleInfoTab={this.toggleInfoTab}
            />
          )}
        </CSSTransitionGroup>
      </div>
    );
  }
}

Chess.propTypes = {
  deckName: PropTypes.string.isRequired,
  movablePiece: PropTypes.shape({
    piece: PropTypes.shape({
      color: PropTypes.string,
      value: PropTypes.string,
    }).isRequired,
    currentCoord: PropTypes.shape({
      x: PropTypes.number,
      y: PropTypes.number,
    }).isRequired,
  }).isRequired,
  pieces: PropTypes.shape({
    white: PropTypes.shape({
      king: PropTypes.number.isRequired,
      queen: PropTypes.number.isRequired,
      rook: PropTypes.number.isRequired,
      bishop: PropTypes.number.isRequired,
      knight: PropTypes.number.isRequired,
      pawn: PropTypes.number.isRequired,
    }).isRequired,
    black: PropTypes.shape({
      king: PropTypes.number.isRequired,
      queen: PropTypes.number.isRequired,
      rook: PropTypes.number.isRequired,
      bishop: PropTypes.number.isRequired,
      knight: PropTypes.number.isRequired,
      pawn: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
  position: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        value: PropTypes.string,
        color: PropTypes.string,
      }).isRequired,
    ).isRequired,
  ).isRequired,
  shouldSelectPiece: PropTypes.func.isRequired,
  shouldMovePiece: PropTypes.func.isRequired,
  returnPiece: PropTypes.func.isRequired,
  shouldSaveDeck: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  pieces: state.pieces,
  position: state.board.position,
  movablePiece: state.game.movablePiece,
  deckName: state.game.deckName,
});

const mapDispatchToProps = dispatch => ({
  returnPiece: bindActionCreators(returnPiece, dispatch),
  shouldMovePiece: bindActionCreators(shouldMovePiece, dispatch),
  shouldSelectPiece: bindActionCreators(shouldSelectPiece, dispatch),
  shouldSaveDeck: bindActionCreators(shouldSaveDeck, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Chess);
