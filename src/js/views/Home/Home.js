import React from 'react';
import { Link } from 'react-router';
import './Home.css';

const Home = () => (
  <div className="home">
    <div className="home__logo">
      <div className="logo">
        <h1 className="logo__header">Немного шахмат?</h1>
        <Link
          to="/chess"
          className="logo__link"
        >
          Play!
        </Link>
      </div>
    </div>
  </div>
);

export default (Home);
