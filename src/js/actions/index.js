import fetch from 'isomorphic-fetch';
import * as actionTypes from 'Constants/ActionTypes';

export const showModal = info => ({
  type: actionTypes.SHOW_MODAL,
  info,
});

export const hideModal = () => ({
  type: actionTypes.HIDE_MODAL,
});

const movePiece = (piece, newCoord, oldCoord) => ({
  type: actionTypes.MOVE_PIECE,
  piece,
  newCoord,
  oldCoord,
});

export const returnPiece = (piece, oldCoord) => ({
  type: actionTypes.RETURN_PIECE,
  piece,
  oldCoord,
});

const selectPiece = (piece, currentCoord) => ({
  type: actionTypes.SELECT_PIECE,
  piece,
  currentCoord,
});

const changeInfo = (deckName, firstTurn) => ({
  type: actionTypes.CHANGE_INFO,
  deckName,
  firstTurn,
});

const requestDeckSave = () => ({
  type: actionTypes.REQUEST_DECK_SAVE,
});

const responseDeckSave = () => ({
  type: actionTypes.RESPONSE_DECK_SAVE,
});

const isCellAvailable = (state, cell, dispatch) => {
  const { position } = state.board;
  if (position[cell.y][cell.x].value) {
    dispatch(showModal('Я бы не ставил эту фигуру сюда!'));
    return false;
  }
  return true;
};

export const shouldMovePiece = (piece, newCoord, oldCoord) => (dispatch, getState) =>
  isCellAvailable(getState(), newCoord, dispatch)
    ? dispatch(movePiece(piece, newCoord, oldCoord))
    : false;

const isPieceSelected = (state, piece, currentCoord) => {
  const prevPiece = state.game.movablePiece.piece;
  const prevCoord = state.game.movablePiece.coord;
  if (typeof prevPiece.value === 'undefined' && typeof prevPiece.color === 'undefined') {
    return false;
  }
  const isPieceTypeSame = piece.value === prevPiece.value && piece.color === prevPiece.color;
  if (currentCoord) {
    if (!prevCoord) return false;
    const isPieceCoordSame = currentCoord.x === prevCoord.x && currentCoord.y === prevCoord.y;
    return isPieceTypeSame && isPieceCoordSame;
  }
  return isPieceTypeSame;
};

export const shouldSelectPiece = (piece, currentCoord) => (dispatch, getState) =>
  isPieceSelected(getState(), piece, currentCoord)
    ? false
    : dispatch(selectPiece(piece, currentCoord));

const bountyHunter = (board) => {
  let whiteKing;
  let blackKing;
  board.forEach((row, rowIndex) =>
    row.forEach((i, columnIndex) => {
      if (i.value === 'king') {
        if (i.color === 'white') {
          whiteKing = { y: rowIndex, x: columnIndex, color: i.color };
        } else {
          blackKing = { y: rowIndex, x: columnIndex, color: i.color };
        }
      }
    }),
  );
  if (whiteKing && blackKing) {
    return [whiteKing, blackKing];
  }
  return false;
};

const checkLines = (lines, kingIndexInLines, dangerousValues, pointColor, isDiagonal) =>
  !kingIndexInLines.some((index, lineNumber) => {
    if (index === -1) {
      return false;
    }
    let i = 1;
    let j = 1;
    while (!(i === 0 && j === 0)) {
      const nextCell = lines[lineNumber][index + i];
      const prevCell = lines[lineNumber][index - j];
      if (!nextCell) i = 0;
      if (!prevCell) j = 0;
      if (nextCell && i !== 0) {
        i += 1;
        const isNextCellEmpty = nextCell.value === null;
        if (
          isDiagonal &&
          i === 2 &&
          nextCell.value === 'pawn' &&
          nextCell.color !== pointColor &&
          pointColor === 'white'
        ) {
          return true;
        }
        if (i === 2 && nextCell.value === 'king' && nextCell.color !== pointColor) {
          return true;
        }
        if (dangerousValues.indexOf(nextCell.value) !== -1 && nextCell.color !== pointColor) {
          return true;
        }
        if (
          !(dangerousValues.indexOf(nextCell.value) !== -1 && nextCell.color !== pointColor) &&
          !isNextCellEmpty
        ) {
          i = 0;
        }
      }
      if (prevCell && j !== 0) {
        j += 1;
        const isPrevCellEmpty = prevCell.value === null;
        if (
          isDiagonal &&
          j === 2 &&
          prevCell.value === 'pawn' &&
          prevCell.color !== pointColor &&
          pointColor === 'black'
        ) {
          return true;
        }
        if (j === 2 && prevCell.value === 'king' && prevCell.color !== pointColor) {
          return true;
        }
        if (dangerousValues.indexOf(prevCell.value) !== -1 && prevCell.color !== pointColor) {
          return true;
        }
        if (
          !(dangerousValues.indexOf(prevCell.value) !== -1 && prevCell.color !== pointColor) &&
          !isPrevCellEmpty
        ) {
          j = 0;
        }
      }
      if (!i && !j) {
        return false;
      }
    }
    return false;
  });

const horizontalLineCheck = (board, point) => {
  const horizontalLines = [];
  horizontalLines.push(board[point.y]);
  horizontalLines.push(board.map(i => i[point.x]));
  const dangerousValues = ['rook', 'queen'];
  const kingIndexInLines = horizontalLines.map(line =>
    line.findIndex(i => i.value === 'king' && i.color === point.color),
  );
  const isDiagonal = false;
  return checkLines(horizontalLines, kingIndexInLines, dangerousValues, point.color, isDiagonal);
};

const diagonalLineCheck = (board, point) => {
  const diagonalLines = [[], []];
  for (let i = point.y, count = 0; i >= 0; i -= 1, count += 1) {
    if (point.x + count < board.length) diagonalLines[0].unshift(board[i][point.x + count]);
    if (point.x - count >= 0) diagonalLines[1].unshift(board[i][point.x - count]);
  }
  for (let i = point.y + 1, count = 1; i < board.length; i += 1, count += 1) {
    if (point.x - count >= 0) diagonalLines[0].push(board[i][point.x - count]);
    if (point.x + count < board.length) diagonalLines[1].push(board[i][point.x + count]);
  }
  const dangerousValues = ['bishop', 'queen'];
  const kingIndexInLines = diagonalLines.map(line =>
    line.findIndex(i => i.value === 'king' && i.color === point.color),
  );
  const isDiagonal = true;
  return checkLines(diagonalLines, kingIndexInLines, dangerousValues, point.color, isDiagonal);
};

const quickAndDirtyHorseCheck = (board, point) => {
  const rows = 8;
  const columns = 8;
  const stables = [];
  const knightMoves = [
    { x: point.x + 2, y: point.y + 1 },
    { x: point.x + 2, y: point.y - 1 },
    { x: point.x + 1, y: point.y + 2 },
    { x: point.x + 1, y: point.y - 2 },
    { x: point.x - 2, y: point.y + 1 },
    { x: point.x - 2, y: point.y - 1 },
    { x: point.x - 1, y: point.y + 2 },
    { x: point.x - 1, y: point.y - 2 },
  ];
  knightMoves.forEach((i) => {
    if (i.x >= 0 && i.x < rows && i.y >= 0 && i.y < columns) stables.push(board[i.y][i.x]);
  });
  return !stables.some(i => i.value === 'knight' && i.color !== point.color);
};

const checkDeserterPawn = board =>
  board[0].some(cell => cell.value === 'pawn') ||
  board[board.length - 1].some(cell => cell.value === 'pawn');

const isKingSafe = (board, point) =>
  horizontalLineCheck(board, point) &&
  diagonalLineCheck(board, point) &&
  quickAndDirtyHorseCheck(board, point);

const validateDeck = (state, dispatch) => {
  const board = state.board.position;
  const areDesertersFound = checkDeserterPawn(board);
  const kings = bountyHunter(board);
  if (!kings) {
    dispatch(showModal('Я привык играть с большим количеством королей!'));
    return false;
  }
  const kingsAreSafe = kings.every(king => isKingSafe(board, king));
  const deckIsValid = kingsAreSafe && !areDesertersFound;
  if (!kingsAreSafe) {
    dispatch(showModal('Мат в один ход! Браво!'));
  } else if (areDesertersFound) {
    dispatch(showModal('Мои пешки обычно в эпицентре событий!'));
  }
  return deckIsValid;
};

const isFetchingInProgress = state => state.game.isFetching;

const saveDeckOnServer = (deckName, firstTurn) => (dispatch, getState) => {
  dispatch(requestDeckSave());
  const state = getState();
  const pieces = state.pieces;
  const board = state.board.position;
  return fetch('/api/test', {
    method: 'POST',
    mode: 'same-origin',
    body: JSON.stringify({
      firstTurn,
      deckName,
      board,
      pieces,
    }),
  })
    .then((response) => {
      dispatch(responseDeckSave());
      if (response.status !== 200) {
        dispatch(showModal('Запрос завершился неудачей, сожалею.'));
      }
      return response.json();
    })
    .then((json) => {
      if (json !== 0) {
        dispatch(showModal('Ваша партия сохранена!'));
      }
    })
    .catch((error) => {
      dispatch(showModal(`Возникли непредвиденные трудности: ${error}`));
    });
};

export const shouldSaveDeck = (deckName, firstTurn) => (dispatch, getState) => {
  dispatch(changeInfo(deckName, firstTurn));
  const isFetching = isFetchingInProgress(getState());
  const deckIsValid = validateDeck(getState(), dispatch);
  return !isFetching && deckIsValid ? dispatch(saveDeckOnServer(deckName, firstTurn)) : false;
};
