module.exports = {
  extends: 'eslint-config-airbnb',
  env: {
    browser: true,
  },
  rules: {
    'max-len': 'off',
    'react/no-array-index-key': 'off',
    'react/jsx-filename-extension': ['error', { extensions: ['.js'] }],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
    'no-confusing-arrow': 'off',
  },
  parser: 'babel-eslint',
  plugins: ['react'],
  settings: {
    'import/resolver': {
      webpack: { config: 'webpack.config.js' },
    },
  },
};
